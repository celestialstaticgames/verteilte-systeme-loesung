# Übungsaufgaben Verteilte Systeme #

In diesem Repository findet ihr die Übungsaufgaben zum Kurs Verteilte Systeme.

## Anweisungen ##

Ihr könnt die Aufgaben mit jeder C#-fähigen IDE öffnen und ausführen. 
Ich kann euch beim Einrichten folgender IDEs unterstützen:

- Visual Studio Code
- JetBrains Rider

Für alle anderen IDEs kann (und werde) ich während der Vorlesung keinen Support anbieten, da müsst ihr selbst zurecht kommen.

## Dependencies ##

Für die Entwicklung von C# mit Visual Studio Code benötigt ihr außerdem folgende Programme/Plugins:

- .NET Core 3.1 SDK (https://dotnet.microsoft.com/download/dotnet-core/3.1)
- C# Extension (https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)