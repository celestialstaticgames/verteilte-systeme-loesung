using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Adventure.Shared;

namespace Adventure.Server
{
    public class SocketConnection
    {
        public Guid ClientId { get; }
        public Socket Client { get; }
        
        private SocketServer _server;

        public SocketConnection(Guid clientId, SocketServer server, Socket client)
        {
            _server = server;
            Client = client;
            ClientId = clientId;

            var thread = new Thread(HandleConnection);
            thread.Start();
        }

        private void HandleConnection()
        {
            try
            {
                while (true)
                {
                    var messageLengthBytes = new byte[4];
                    byte[] messageBytes = null;

                    var bytesRec = Client.Receive(messageLengthBytes);
                    var messageLength = BitConverter.ToInt32(messageLengthBytes);

                    if (bytesRec != 4)
                    {
                        Console.WriteLine($"Error receiving packet length. Expected 4, but got {bytesRec}.");
                    }

                    messageBytes = new byte[messageLength];
                    bytesRec = Client.Receive(messageBytes);
                    var data = Encoding.ASCII.GetString(messageBytes, 0, bytesRec);

                    _server.OnMessageReceived(this, data);
                }
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.ConnectionReset)
                {
                    _server.OnClientDisconnected(this);
                }
            }
        }
    }

    public abstract class SocketServer
    {
        private readonly List<SocketConnection> _connections = new List<SocketConnection>();

        public void StartServer(string hostname, int port)
        {
            var host = Dns.GetHostEntry(hostname);
            var ipAddress = host.AddressList[0];
            var localEndPoint = new IPEndPoint(ipAddress, port);

            Console.WriteLine("Starting server...");

            try
            {
                var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(localEndPoint);
                listener.Listen(10);

                Console.WriteLine($"Server listening on {hostname}:{port}.");

                StartListening(listener);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during server socket setup: {e.Message}");
            }
        }

        private void StartListening(Socket listener)
        {
            try
            {
                while (true)
                {
                    Console.WriteLine("Listening for connection...");

                    var client = listener.Accept();
                    client.ReceiveTimeout = -1;
                    
                    var connection = new SocketConnection(Guid.NewGuid(), this, client);
                    _connections.Add(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during accept loop: {e.Message}");
            }

            StartListening(listener);
        }

        public void OnClientDisconnected(SocketConnection connection)
        {
            if (_connections.Contains(connection))
            {
                _connections.Remove(connection);
            }
            
            Console.WriteLine($"Client {connection.ClientId} disconnected.");
        }

        public abstract void OnMessageReceived(SocketConnection connection, string message);

        protected void SendMessage(Socket client, string message)
        {
            var responseBuffer = new byte[1024];
            var msg = (byte[]) Encoding.ASCII.GetBytes(message);
            var messageLengthBytes = BitConverter.GetBytes(message.Length);

            client.Send(messageLengthBytes);
            client.Send(msg);
        }
    }
}