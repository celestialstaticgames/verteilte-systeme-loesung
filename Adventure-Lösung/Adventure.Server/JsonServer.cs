using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Adventure.Server.Game;
using Adventure.Shared.Commands;
using Newtonsoft.Json;

namespace Adventure.Server
{
    public class JsonServer : SocketServer, ICommandSender
    {
        private const string CallToActionText = "Was tust du?";
        
        private readonly Dictionary<Guid, MainGame> _runningGames = new Dictionary<Guid, MainGame>();
        
        public override void OnMessageReceived(SocketConnection connection, string message) {
            var command = (ICommand) JsonConvert.DeserializeObject(message, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.All
            });

            var game = GetOrCreateGame(connection.ClientId);

            if (command is ClientConnectedCommand)
            {
                game.Start(connection);
            }
            
            //game.OnCommandReceived(command);
        }

        public void Send(ICommand command, Socket receiver) {
            SendMessage(receiver, JsonConvert.SerializeObject(command, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            }));
        }

        private MainGame GetOrCreateGame(Guid clientId)
        {
            if (!_runningGames.TryGetValue(clientId, out var game))
            {
                game = CreateGame();
                _runningGames.Add(clientId, game);
                
                return game;
            }
            
            Console.WriteLine($"Found existing game for client {clientId}.");

            if (game.Status == GameStatus.Aborted || game.Status == GameStatus.Finished)
            {
                Console.WriteLine($"Game was aborted or finished for client {clientId}, will restart.");
                
                FinalizeGame(game);
                _runningGames.Remove(clientId);
                
                game = CreateGame();
                _runningGames.Add(clientId, game);
            }

            return game;
        }

        private MainGame CreateGame()
        {
            var game = new MainGame();
            game.OnEnterScene += OnEnterScene;

            return game;
        }

        private void FinalizeGame(MainGame game)
        {
            game.OnEnterScene -= OnEnterScene;
        }

        private void OnEnterScene(MainGame game, Scene scene)
        {
            Console.WriteLine($"[{game.Client.ClientId}] - OnEnterScene: {scene.Id}");
            Send(new PrintTextCommand($"{scene.Description}\n\n{CallToActionText}"), game.Client.Client);
            Send(new TextInputCommand(), game.Client.Client);
        }
    }
}
