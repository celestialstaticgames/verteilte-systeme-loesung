﻿using System;
using System.Collections.Generic;
using Action = Adventure.Shared.Actions.Action;

namespace Adventure.Server.Game
{
    public class Scene
    {
        public string Id { get; }
        public string Description { get; }
        public Inventory Inventory { get; } = new Inventory();
        public IEnumerable<Action> Actions => _actions;

        private readonly List<Action> _actions = new List<Action>();

        public Scene(string id, string description, IEnumerable<Action> actions)
        {
            Id = id;
            Description = description;
            _actions.AddRange(actions);
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}