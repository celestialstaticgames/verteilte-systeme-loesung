﻿namespace Adventure.Server.Game
{
    public class Player
    {
        public string Name { get; set; }
        
        public Inventory Inventory { get; }
    }
}