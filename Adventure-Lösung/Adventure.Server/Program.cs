﻿namespace Adventure.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new JsonServer();
            server.StartServer("localhost", 8080);
        }
    }
}
