using System;

namespace Adventure.Server
{
    public class Command
    {
        public string Id {get;set;}
        public string[] Arguments {get;set;}
    }
}
