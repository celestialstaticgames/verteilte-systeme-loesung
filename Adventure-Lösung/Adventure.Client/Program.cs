﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Adventure.Shared.Commands;

namespace Adventure.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new JsonClient();
            client.StartClient("localhost", 8080);
        }
    }
}
