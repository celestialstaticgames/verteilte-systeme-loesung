using System;
using System.Net;
using System.Net.Sockets;
using Adventure.Shared.Commands;
using Newtonsoft.Json;

namespace Adventure.Client
{
    public class JsonClient : SocketClient, ICommandSender
    {
        private JsonSerializerSettings _settings = new JsonSerializerSettings {
            TypeNameHandling = TypeNameHandling.All
        };

        public void Send(ICommand command, Socket responseReceiver)
        {
            // Since we're storing a single reference to the server, responseReceiver stays unused on the client.
            SendMessage(JsonConvert.SerializeObject(command, Formatting.Indented, _settings));
        }

        protected override void OnMessageReceived(Socket handler, string message)
        {
            var command = (ICommand)JsonConvert.DeserializeObject(message, _settings);

            command.ExecuteClient(this, handler);
        }

        protected override void SendInitialMessage()
        {
            Send(new ClientConnectedCommand(), null);
        }
    }
}
