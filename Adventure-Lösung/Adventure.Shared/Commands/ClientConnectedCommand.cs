using System;
using System.Net.Sockets;

namespace Adventure.Shared.Commands
{
    public class ClientConnectedCommand : ICommand
    {
        public void ExecuteClient(ICommandSender sender, Socket responseReceiver)
        {
        }

        public void ExecuteServer(ICommandSender sender, Socket responseReceiver)
        {
        }
    }
}