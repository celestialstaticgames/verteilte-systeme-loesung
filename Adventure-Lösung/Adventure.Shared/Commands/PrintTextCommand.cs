using System;
using System.Net.Sockets;

namespace Adventure.Shared.Commands
{
    public class PrintTextCommand : ICommand
    {

        public string Text { get; }

        public PrintTextCommand(string text) {
            Text = text;
        }

        public void ExecuteClient(ICommandSender sender, Socket responseReceiver)
        {
            Console.WriteLine(Text);
        }

        public void ExecuteServer(ICommandSender sender, Socket responseReceiver)
        {
            throw new NotImplementedException();
        }
    }
}
