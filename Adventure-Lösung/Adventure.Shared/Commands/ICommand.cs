using System;
using System.Net.Sockets;

namespace Adventure.Shared.Commands
{
    public interface ICommand
    {
        void ExecuteClient(ICommandSender sender, Socket responseReceiver);

        void ExecuteServer(ICommandSender sender, Socket responseReceiver);
    }
}
