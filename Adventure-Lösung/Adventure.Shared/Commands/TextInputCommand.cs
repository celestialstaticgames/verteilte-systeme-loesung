using System;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Adventure.Shared.Commands
{
    public class TextInputCommand : ICommand
    {
        [JsonProperty]
        public string Response {get; private set;}

        public void ExecuteClient(ICommandSender sender, Socket responseReceiver)
        {
            Response = Console.ReadLine();
            sender.Send(this, responseReceiver);
        }

        public void ExecuteServer(ICommandSender sender, Socket responseReceiver)
        {
            Console.WriteLine($"Player name is {Response}.");
            sender.Send(new PrintTextCommand($"Welcome to your adventure, {Response}. The world awaits you..."), responseReceiver);
        }
    }
}
