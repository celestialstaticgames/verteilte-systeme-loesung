using System.Net.Sockets;

namespace Adventure.Shared.Commands
{
    public interface ICommandSender
    {
        void Send(ICommand command, Socket receiver);
    }
}
