﻿using System.Collections.Generic;

namespace Adventure.Shared.Actions
{
    public class Action
    {
        public string Verb { get; }

        private IEnumerable<string> _allowedParameters;

        public Action(string verb, params string[] allowedParameters)
        {
            Verb = verb;
            _allowedParameters = allowedParameters;
        }

        public void Perform(string[] parameters)
        {
            
        }
    }
}